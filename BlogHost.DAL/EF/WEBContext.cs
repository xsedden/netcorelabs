﻿using System;
//using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using BlogHost.DAL.Entities;
using BlogHost.DAL.Configurations;


namespace BlogHost.DAL.EF
{
    public class UserContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Role> Roles { get; set; }

        public UserContext(DbContextOptions<UserContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        /*public UserContext(string dbConnection) : base(dbConnection)
        {
        }*/

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            string adminRoleName = "admin";
            string userRoleName = "user";

            string adminEmail = "admin@mail.ru";
            string adminPassword = "123456";

            // добавляем роли
            Role adminRole = new Role { Id = 1, Name = adminRoleName };
            Role userRole = new Role { Id = 2, Name = userRoleName };

            User adminUser = new User { Id = 1, Email = adminEmail, Password = adminPassword, RoleId = adminRole.Id, Login = "admin" };

            Blog Blog1 = new Blog { Id = 1, Name = "Blog1", UserId = 1, CreationDate = DateTime.Now };
            Blog Blog2 = new Blog { Id = 2, Name = "Blog2", UserId = 1, CreationDate = DateTime.Now };
            Blog Blog3 = new Blog { Id = 3, Name = "Blog3", UserId = 1, CreationDate = DateTime.Now };
            Blog Blog4 = new Blog { Id = 4, Name = "Blog4", UserId = 1, CreationDate = DateTime.Now };

            modelBuilder.Entity<Blog>().HasData(new Blog[] { Blog1, Blog2, Blog3, Blog4 });
            modelBuilder.Entity<Role>().HasData(new Role[] { adminRole, userRole });
            modelBuilder.Entity<User>().HasData(new User[] { adminUser });

            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new BlogConfiguration());
            modelBuilder.ApplyConfiguration(new PostConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());


            base.OnModelCreating(modelBuilder);
        }
    }
}
