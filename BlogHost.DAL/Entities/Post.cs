﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogHost.DAL.Entities
{
    public class Post
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }

        public int? BlogId { get; set; }
        public virtual Blog Blog { get; set; }

        public int? UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }

        public Post()
        {
            Comments = new List<Comment>();
            Tags = new List<Tag>();
        }
    }
}
