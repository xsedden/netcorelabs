﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogHost.DAL.Entities
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? PostId { get; set; }
        public virtual Post post { get; set; }
    }
}
