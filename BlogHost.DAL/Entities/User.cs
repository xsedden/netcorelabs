﻿using System;
using System.Collections.Generic;
using System.Text;
//using Microsoft.AspNetCore.Identity;

namespace BlogHost.DAL.Entities
{
    public class User 
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public int? RoleId { get; set; }
        public virtual Role Role { get; set; }

        public virtual ICollection<Blog> Blogs { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public User()
        {
            Comments = new List<Comment>();
            Blogs = new List<Blog>();
            Posts = new List<Post>();
        }
    }
}