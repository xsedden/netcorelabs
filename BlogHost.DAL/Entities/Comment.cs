﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogHost.DAL.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }

        public int? PostId { get; set; }
        public virtual Post Post { get; set; }

        public int? UserId { get; set; }
        public virtual User User { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
