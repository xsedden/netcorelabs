﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.Entities;

namespace BlogHost.DAL.Configurations
{
    class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(user => user.Email).IsRequired();
            builder.Property(user => user.Login).IsRequired();
            builder.Property(user => user.Password).IsRequired();
            builder.HasMany(user => user.Blogs)
                .WithOne(blog => blog.User)
                .HasForeignKey(blog => blog.UserId)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasMany(user => user.Comments)
                .WithOne(comm => comm.User)
                .HasForeignKey(comm => comm.UserId)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasMany(user => user.Posts)
                .WithOne(post => post.User)
                .HasForeignKey(post => post.UserId)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
