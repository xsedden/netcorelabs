﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.Entities;

namespace BlogHost.DAL.Configurations
{
    class PostConfiguration : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.Property(post => post.Name).IsRequired();
            builder.Property(post => post.Text).IsRequired();
            builder.HasMany(post => post.Comments)
                .WithOne(comment => comment.Post)
                .HasForeignKey(comment => comment.PostId)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasMany(post => post.Tags)
                .WithOne(tag => tag.post)
                .HasForeignKey(tag => tag.PostId)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
