﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.EF;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;


namespace BlogHost.DAL.Repositoriers
{
    class CommentRepository : ICommentProvider
    {
        private UserContext db;

        public CommentRepository(UserContext context)
        {
            this.db = context;
        }

        public IEnumerable<Comment> GetAll()
        {
            return db.Comments;
        }

        public Comment Get(int id)
        {
            return db.Comments.Find(id);
        }

        public void Create(Comment comment)
        {
            db.Comments.Add(comment);
            db.SaveChanges();
        }

        public void Update(Comment comment)
        {
            db.Entry(comment).State = EntityState.Modified;
            db.SaveChanges();
        }

        /*
        public IEnumerable<Blog> Find(Func<Blog, Boolean> predicate)
        {
            return db.Blogs.Where(predicate).ToList();
        }
        */

        public void Delete(int id)
        {
            Comment com = db.Comments.Find(id);
            if (com != null)
            {
                db.Comments.Remove(com);
                db.SaveChanges();
            }
        }

        public IEnumerable<Comment> GetCommentsByPost(int id)
        {
            return db.Comments.Where(com => com.PostId == id).ToList();
        }
       

        public IEnumerable<Comment> GetCommentsByAuthor(int id)
        {
            return db.Comments.Where(com => com.UserId == id).ToList();
        }

    }
}
