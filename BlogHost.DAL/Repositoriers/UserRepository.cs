﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.EF;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;


namespace BlogHost.DAL.Repositoriers
{
    class UserRepository : IUserProvider
    {
        private UserContext db;

        public UserRepository(UserContext context)
        {
            this.db = context;
        }

        public IEnumerable<User> GetAll()
        {
            return db.Users;
        }

        public User Get(int id)
        {
            return db.Users.Find(id);
        }

        public void Create(User user)
        {
            db.Users.Add(user);
            db.SaveChanges();
        }

        public void Update(User user)
        {
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
        }

        /*
        public IEnumerable<Blog> Find(Func<Blog, Boolean> predicate)
        {
            return db.Blogs.Where(predicate).ToList();
        }
        */

        public void Delete(int id)
        {
            User user = db.Users.Find(id);
            if (user != null)
            {
                db.Users.Remove(user);
                db.SaveChanges();
            }
        }

        public IEnumerable<Tag> GetTagsByPost(int id)
        {
            return db.Tags.Where(tag => tag.PostId == id).ToList();
        }

        public Tag GetTagByName(string name)
        {
            return db.Tags.FirstOrDefault(tag => tag.Name == name);
        }

        public User GetUserByPost(int id)
        {
            return db.Users.FirstOrDefault(user => user.Posts.Any(post => post.Id == id));
        }

        public User GetUserByBlog(int id)
        {
            return db.Users.FirstOrDefault(user => user.Blogs.Any(blog => blog.Id == id));
        }

        public User GetUserByComment(int id)
        {
            return db.Users.FirstOrDefault(user => user.Comments.Any(com => com.Id == id));
        }

        public IEnumerable<User> GetAdmins()
        {
            return db.Users.Where(user => user.RoleId == 1).ToList();
        }

        public IEnumerable<User> GetUsers()
        {
            return db.Users.Where(user => user.RoleId == 2).ToList();
        }
    }
}
