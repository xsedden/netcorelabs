﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.EF;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace BlogHost.DAL.Repositoriers
{
    public class RoleRepository : IRoleProvider
    {
        private UserContext db;

        public RoleRepository(UserContext context)
        {
            this.db = context;
        }

        public Role GetUserRole()
        {
            return db.Roles.Find(2);
        }

        public Role GetAdminRole()
        {
            return db.Roles.Find(1);
        }
    }
}
