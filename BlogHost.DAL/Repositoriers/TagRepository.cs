﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.EF;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;


namespace BlogHost.DAL.Repositoriers
{
    class TagRepository : ITagProvider
    {
        private UserContext db;

        public TagRepository(UserContext context)
        {
            this.db = context;
        }

        public IEnumerable<Tag> GetAll()
        {
            return db.Tags;
        }

        public Tag Get(int id)
        {
            return db.Tags.Find(id);
        }

        public void Create(Tag tag)
        {
            db.Tags.Add(tag);
            db.SaveChanges();
        }

        public void Update(Tag tag)
        {
            db.Entry(tag).State = EntityState.Modified;
            db.SaveChanges();
        }

        /*
        public IEnumerable<Blog> Find(Func<Blog, Boolean> predicate)
        {
            return db.Blogs.Where(predicate).ToList();
        }
        */

        public void Delete(int id)
        {
            Tag tag = db.Tags.Find(id);
            if (tag != null)
            {
                db.Tags.Remove(tag);
                db.SaveChanges();
            }
        }

        public IEnumerable<Tag> GetTagsByPost(int id)
        {
            return db.Tags.Where(tag => tag.PostId == id).ToList();
        }

        public Tag GetTagByName(string name)
        {
            return db.Tags.FirstOrDefault(tag => tag.Name == name);
        }

      
    }
}
