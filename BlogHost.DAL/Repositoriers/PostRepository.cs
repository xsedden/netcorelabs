﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.EF;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;


namespace BlogHost.DAL.Repositoriers
{
    class PostRepository : IPostProvider
    {
        private UserContext db;

        public PostRepository(UserContext context)
        {
            this.db = context;
        }

        public IEnumerable<Post> GetAll()
        {
            return db.Posts;
        }

        public Post Get(int id)
        {
            return db.Posts.Find(id);
        }

        public void Create(Post post)
        {
            db.Posts.Add(post);
            db.SaveChanges();
        }

        public void Update(Post post)
        {
            db.Entry(post).State = EntityState.Modified;
            db.SaveChanges();
        }

        /*
        public IEnumerable<Blog> Find(Func<Blog, Boolean> predicate)
        {
            return db.Blogs.Where(predicate).ToList();
        }
        */

        public void Delete(int id)
        {
            Post post = db.Posts.Find(id);
            if (post != null)
            {
                db.Posts.Remove(post);
                db.SaveChanges();
            }
        }

        public IEnumerable<Post> GetPostsForTag(int id)
        {
            return db.Posts.Where(post => post.Tags.Any(tag => tag.Id == id)).ToList();
        }

        public IEnumerable<Post> GetPostsForAuthor(int id)
        {
            return db.Posts.Where(post => post.UserId == id).ToList();
        }

        public IEnumerable<Post> GetPostsForBlog(int id)
        {
            return db.Posts.Where(post => post.BlogId == id).ToList();
        }

        public IEnumerable<Post> GetPostsBySearchRequest(string request)
        {
            return db.Posts.Where(post => post.Name.ToLower().Contains(request)
                    || string.Join(' ', post.Text).Contains(request)).ToList();
        }

    }
}
