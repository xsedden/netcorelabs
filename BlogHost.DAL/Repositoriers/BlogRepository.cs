﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.EF;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace BlogHost.DAL.Repositoriers
{

    public class BlogRepository : IBlogProvider
    {
        private UserContext db;

        public BlogRepository(UserContext context)
        {
            this.db = context;
        }

        public IEnumerable<Blog> GetAll()
        {
            return db.Blogs;
        }

        public Blog Get(int id)
        {
            return db.Blogs.Find(id);
        }

        public void Create(Blog blog)
        {
            db.Blogs.Add(blog);
            db.SaveChanges();
        }

        public void Update(Blog blog)
        {
            db.Entry(blog).State = EntityState.Modified;
            db.SaveChanges();
        }

        /*public IEnumerable<Blog> Find(Func<Blog, Boolean> predicate)
        {
            return db.Blogs.Where(predicate).ToList();
        }*/

        public void Delete(int id)
        {
            Blog blog = db.Blogs.Find(id);
            if (blog != null)
            {
                db.Blogs.Remove(blog);
                db.SaveChanges();
            }
        }

        public IEnumerable<Blog> GetBlogsForAuthor(int id)
        {
            return db.Blogs.Where(blog => blog.UserId == id).ToList();
        }

        public IEnumerable<Blog> GetBlogsBySearchRequest(string request)
        {
            return db.Blogs.Where(blog => blog.Name.ToLower().Contains(request));               
        }
    }
}

