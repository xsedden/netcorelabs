﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.EF;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;

    namespace BlogHost.DAL.Repositoriers
    {
        public class EFUnitOfWork : IUnitOfWork
        {
            private UserContext db;
            private BlogRepository blogRepository;
            private PostRepository postRepository;
            private CommentRepository comRepository;
            private TagRepository tagRepository;
            private UserRepository userRepository;
            private RoleRepository roleRepository;

            /*public EFUnitOfWork(string connectionString)
            {
                db = new UserContext(connectionString);
            }*/

            public EFUnitOfWork(UserContext context)
            {
                db = context;
            }

            public ITagProvider Tags
            {
                get
                {
                    if (tagRepository == null)
                        tagRepository = new TagRepository(db);
                    return tagRepository;
                }
            }

            public ICommentProvider Comments
            {
                get
                {
                    if (comRepository == null)
                        comRepository = new CommentRepository(db);
                    return comRepository;
                }
            }

            public IBlogProvider Blogs
            {
                get
                {
                    if (blogRepository == null)
                        blogRepository = new BlogRepository(db);
                    return blogRepository;
                }
            }

            public IPostProvider Posts
            {
                get
                {
                    if (postRepository == null)
                        postRepository = new PostRepository(db);
                    return postRepository;
                }
            }

            public IUserProvider Users
            {
                get
                {
                    if (userRepository == null)
                        userRepository = new UserRepository(db);
                    return userRepository;
                }
            }

            public IRoleProvider Roles
            {
                get
                {
                    if (roleRepository == null)
                        roleRepository = new RoleRepository(db);
                    return roleRepository;
                }
            }

            public void Save()
            {
                db.SaveChanges();
            }

            private bool disposed = false;

            public virtual void Dispose(bool disposing)
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        db.Dispose();
                    }
                    this.disposed = true;
                }
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }
    }

