﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.Entities;

namespace BlogHost.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IBlogProvider Blogs { get; }
        ICommentProvider Comments { get; }
        IPostProvider Posts { get; }
        IRoleProvider Roles { get; }
        ITagProvider Tags { get; }
        IUserProvider Users { get; }
        void Save();
    }
}
