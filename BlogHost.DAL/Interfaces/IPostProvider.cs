﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.EF;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;

namespace BlogHost.DAL.Interfaces
{
    public interface IPostProvider : IRepository<Post> 
    {
        IEnumerable<Post> GetPostsForTag(int id);

        IEnumerable<Post> GetPostsForBlog(int id);

        IEnumerable<Post> GetPostsForAuthor(int id);

        IEnumerable<Post> GetPostsBySearchRequest(string request);
    }
}
