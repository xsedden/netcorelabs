﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.EF;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;


namespace BlogHost.DAL.Interfaces
{
    public interface IUserProvider : IRepository<User>
    {
        User GetUserByPost(int id);

        User GetUserByBlog(int id);

        User GetUserByComment(int id);

        IEnumerable<User> GetAdmins();

        IEnumerable<User> GetUsers();
    }
}
