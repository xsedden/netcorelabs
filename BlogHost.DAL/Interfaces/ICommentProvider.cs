﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.EF;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;


namespace BlogHost.DAL.Interfaces
{
    public interface ICommentProvider : IRepository<Comment>
    {
        IEnumerable<Comment> GetCommentsByPost(int id);

        IEnumerable<Comment> GetCommentsByAuthor(int id);

    }
}
