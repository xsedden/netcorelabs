﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.EF;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;


namespace BlogHost.DAL.Interfaces
{
    public interface ITagProvider : IRepository<Tag>
    {
        IEnumerable<Tag> GetTagsByPost(int id);

        Tag GetTagByName(string name);

    }
}
