﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.EF;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;


namespace BlogHost.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        //IEnumerable<T> Find(Func<T, Boolean> predicate);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
    }
}
