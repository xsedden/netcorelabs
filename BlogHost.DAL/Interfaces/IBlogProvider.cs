﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.DAL.EF;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;

namespace BlogHost.DAL.Interfaces
{
    public interface IBlogProvider : IRepository<Blog>
    {

        IEnumerable<Blog> GetBlogsForAuthor(int id);

        IEnumerable<Blog> GetBlogsBySearchRequest(string request);

    }
}
