﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Tags
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PostId { get; set; }
        public Post post { get; set; }
    }
}
