﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace WebApplication1.Controllers
{
    public class ChatController : Controller
    {
        [Route("/Chat/Index")]
        [Authorize(Roles = "1, 2")]
        public IActionResult Index()
        {
            return View();
        }
    }
}