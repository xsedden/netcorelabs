﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication1.Models;
using WebApplication1.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using BlogHost.BLL.Interfaces;
using BlogHost.BLL.DTO;

namespace WebApplication1.Controllers
{ 

    public class PostsController : Controller
    {
        IUnit db { get; set; }

        public PostsController(IUnit uow)
        {
            db = uow;
        }

        [HttpGet("/Posts/{id:int?}", Name = "Posts")]
        public IActionResult Index(int? id)
        {
            ViewData["BlogId"] = id;
            /*if (id == null)
            {
                var posts = db.Posts.GetPostsAll();
                return View(posts);
            }
            var post = db.Posts.GetPostsByBlog((int)id);*/
            return View();//post);
        }


        [Authorize(Roles = "1, 2")]
        [HttpGet("/Posts/create/{id:int?}", Name = "createpost")]
        public IActionResult Create()
        {    
            return View();
        }

        [HttpPost("/Posts/create/{id:int?}", Name = "createpost")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "1, 2")]
        public IActionResult Create(int? id, CreatePostModel model)
        {
            if (db.Users.GetUserById((int)db.Blogs.GetBlogByID((int)id).UserId).Email == User.Identity.Name)
            { 
                if (ModelState.IsValid)
                {                   
                    var post = new PostDTO { Name = model.Name, CreationDate = DateTime.Now, BlogId = (int)id, Text = model.Text, UserId = db.Users.GetIdByEmail(User.Identity.Name),  };
                    db.Posts.SavePost(post);
                    return RedirectToAction("Index", "Blog");
                }
                return View(model);
            }   
            return RedirectToAction("Index", "Blog");
        }

        [HttpGet("/Posts/info/{id:int}", Name = "AboutPost")]
        public IActionResult Info(int id)
        {
            var post = db.Posts.GetPostById(id);
            ViewData["PostsName"] = post.Name;
            ViewData["PostsText"] = post.Text;
            ViewData["PostsId"] = id;
            return View();
        }

        [HttpPost("/Posts/info/{id:int}", Name = "AboutPost")]
        [ValidateAntiForgeryToken]
        public IActionResult Info(int id, CreateCommentsModel model)
        {
            var post = db.Posts.GetPostById(id);
            ViewData["PostsName"] = post.Name;
            ViewData["PostsText"] = post.Text;
            ViewData["PostsId"] = id;
            if (ModelState.IsValid && User.Identity.IsAuthenticated)
            {
                CommentDTO com = new CommentDTO { Text = model.Text, CreationDate = DateTime.Now, PostId = id, UserId = db.Users.GetIdByEmail(User.Identity.Name) };
                db.Comments.SaveComment(com);
            }
            return View(model);
        }


        [HttpGet("/Posts/delete/{id:int}", Name = "deletePost")]
        [Authorize(Roles = "1, 2")]
        public IActionResult Delete(int id)
        {
            //////// чекнуть какой ид передается эт может быть ид блога///всё норм) 
            if (db.Users.GetUserById((int)db.Posts.GetPostById(id).UserId).Email == User.Identity.Name)
            {
                db.Posts.DeletePost(id);
            }
            return RedirectToAction(nameof(Index));
        }

        /*public async Task<IActionResult> Index(int id)
        {
            return View();
        }*/

        // GET: Blogs/Edit/5
        [HttpGet("/Posts/edit/{id:int?}", Name = "editPost")]
        public IActionResult Edit(int? id, CreatePostModel model)
        {
            PostDTO post = db.Posts.GetPostById((int)id);
            model.Name = post.Name;
            model.Text = post.Text;
            return View(model);
        }

        // POST: Blogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost("/Posts/edit/{id:int?}", Name = "editPost")]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, CreatePostModel model)
        {
            if (ModelState.IsValid)
            {
                PostDTO post = db.Posts.GetPostById(id);
                post.Name = model.Name;
                post.Text = model.Text;
                db.Posts.UpdatePost(post);
            }
            return View(model);
        }
    }
}