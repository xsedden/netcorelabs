﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using WebApplication1.ViewModels;
using System.Security.Claims;
using BlogHost.BLL.Interfaces;
using BlogHost.BLL.DTO;
using AutoMapper;
//using System.Web.UI;
//using System.Web.Script.Serialization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1.Controllers
{
    //[Route("api/[controller]")]
    [Produces("application/json")]
    public class APISearchController : Controller
    {
        IUnit db { get; set; }

        public APISearchController(IUnit uow)
        {
            db = uow;
        }


        [HttpGet("/api/search")]
        public IActionResult Get(string searchstring)
        {

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<BlogDTO, ViewBlogsModel>().ForMember("Author", opt => opt.MapFrom(c => db.Users.GetUserById((int)c.UserId).Login))).CreateMapper();
            List<ViewBlogsModel> blogs = mapper.Map<IEnumerable<BlogDTO>, List<ViewBlogsModel>>(db.Blogs.GetAllBlogs());

            if (!String.IsNullOrEmpty((string)searchstring))
            {
                blogs = mapper.Map<IEnumerable<BlogDTO>, List<ViewBlogsModel>>(db.Blogs.GetBlogsBySearchRequest((string)searchstring));
            }
           
            List<object> blogs2 = new List<object>();
            foreach (var t in blogs)
            {
                blogs2.Add(fun(t));
            }
    
            return Ok(blogs2);
        }


        [HttpGet("/api/searchposts")]
        public IActionResult SearchPosts(string searchstring, int? blogid)
        {

            List<PostDTO> posts = db.Posts.GetPostsAll();
            
            if (!String.IsNullOrEmpty(searchstring))
            {
                posts =db.Posts.GetPostsBySearchRequest(searchstring);
            }
            if (blogid != -1)
            {
                posts = posts.Where(u => u.BlogId == blogid).ToList();
            }

            List<object> posts2 = new List<object>();
            foreach (var t in posts)
            {
                posts2.Add(PostToObj(t));
            }


            return Ok(posts2);

        }

        public IActionResult SearchPosts(string searchstring)
        {

            List<PostDTO> posts = db.Posts.GetPostsAll();

            if (!String.IsNullOrEmpty(searchstring))
            {
                posts = db.Posts.GetPostsBySearchRequest(searchstring);
            }

            List<object> posts2 = new List<object>();
            foreach (var t in posts)
            {
                posts2.Add(PostToObj(t));
            }


            return Ok(posts2);

        }

        private object PostToObj(PostDTO post)
        {
            object obj = new { id = post.Id, author = db.Users.GetUserById((int)post.UserId).Login, name = post.Name, creationdate = post.CreationDate.ToString("F") };
            return obj;
        }


        private object fun(ViewBlogsModel par)
        {
            object res = new { id = par.Id, author = par.Author, name = par.Name, creationdate = par.CreationDate.ToString("F") };
            return res;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
