﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
//using WebApplication1.Models;
using WebApplication1.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using BlogHost.BLL.Interfaces;
using BlogHost.BLL.DTO;
using AutoMapper;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1.Controllers
{
    public class BlogController : Controller
    {
        IUnit db { get; set; }

        public BlogController(IUnit uow)
        {
            db = uow;
        }
        // GET: /<controller>/
        /*public IActionResult Index()
        {
            return View(db.Blogs.ToList());
        }*/
        [Route("/Blog/All")]
        public IActionResult Index()//string searchString)
        {/*
            //var blog = db.Blogs.GetAllBlogs();

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<BlogDTO, ViewBlogsModel>().ForMember("Author", opt => opt.MapFrom(c => db.Users.GetUserById((int)c.UserId).Login))).CreateMapper();
            List<ViewBlogsModel> blogs =  mapper.Map<IEnumerable<BlogDTO>, List<ViewBlogsModel>>(db.Blogs.GetAllBlogs());

            if (!String.IsNullOrEmpty(searchString))
            {
                blogs = mapper.Map<IEnumerable<BlogDTO>, List<ViewBlogsModel>>(db.Blogs.GetBlogsBySearchRequest(searchString));
            }

            return View(blogs);
            //var blogHostingContext = db.Blogs.Include(b => b.User);
            //return View(await blogHostingContext.ToListAsync());*/
            return View();
        }

        [HttpGet("/Blog/create", Name = "createblog")]
        [Authorize(Roles = "1, 2")]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost("/Blog/create", Name = "createblog")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "1, 2")]
        public IActionResult Create(CreateBlogModel model)
        {
            if (ModelState.IsValid)
            {                    
                BlogDTO blog = new BlogDTO { Name = model.Name, CreationDate = DateTime.Now, UserId = db.Users.GetIdByEmail(User.Identity.Name),};
                db.Blogs.SaveBlog(blog);
                return RedirectToAction("Index", "Blog");
            }
            
            return View(model);
        }

        [HttpGet("/Blog/delete/{id:int}", Name = "deleteblog")]
        [Authorize(Roles = "1, 2")]
        public IActionResult Delete(int id)
        {     
            if (db.Users.GetUserById((int)db.Blogs.GetBlogByID(id).UserId).Email == User.Identity.Name)
            {
                db.Blogs.DeleteBlog(id);
            }
            return RedirectToAction(nameof(Index));
        }

    }
}
