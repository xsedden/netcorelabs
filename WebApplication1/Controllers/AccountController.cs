﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using WebApplication1.ViewModels; // пространство имен моделей RegisterModel и LoginModel
using WebApplication1.Models; // пространство имен UserContext и класса User
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using WebApplication1.Util;
using BlogHost.BLL.Interfaces;
using BlogHost.BLL.DTO;

namespace WebApplication1.Controllers
{

    public class AccountController : Controller
    {
        IUnit _context { get; set; }

        public AccountController(IUnit uow)
        {
            _context = uow;
        }

        [HttpGet("/register", Name = "register")]
        public IActionResult Register()
        {
            
            return View();
        }
        [HttpPost("/register", Name = "register")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            
            if (ModelState.IsValid)
            {
                    // добавляем пользователя в бд
                var user = new UserDTO { Email = model.Email, Password = model.Password,  RoleId = 2, Login = model.Login };
                _context.Users.SaveUser(user);

                await Authenticate(user);
                    //await Authenticate(user); // аутентификация

                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }
        [HttpGet("/login", Name = "login")]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost("/login", Name = "login")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                UserDTO user = _context.Users.AuthLogin(model.Password, model.Email);
                if (user != null)
                {
                    await Authenticate(user); // аутентификация

                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }

        [Route("/logout", Name = "logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Home");
        }

        private async Task Authenticate(UserDTO user)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, (user.RoleId).ToString())
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
    }
}