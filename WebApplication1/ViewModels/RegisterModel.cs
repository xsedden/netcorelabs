﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.ViewModels
{
    public class RegisterModel
    {
        [StringLength(20, ErrorMessage = "EmailLength", MinimumLength = 5)]
        [Required(ErrorMessage = "EmailRequired")]
        public string Email { get; set; }

        [StringLength(10, ErrorMessage = "LoginLength", MinimumLength = 4)]
        [Required(ErrorMessage = "LoginRequired")]
        public string Login { get; set; }

        [StringLength(15, ErrorMessage = "PassLength", MinimumLength = 5)]
        [Required(ErrorMessage = "PassRequired")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [StringLength(15, ErrorMessage = "RePassLength", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "RePassRequired")]
        public string ConfirmPassword { get; set; }
    }
}
