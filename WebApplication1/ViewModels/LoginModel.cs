﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.ViewModels
{
    public class LoginModel
    {
        [StringLength(20, ErrorMessage = "EmailLength", MinimumLength = 5)]
        [Required(ErrorMessage = "EmailRequired")]
        public string Email { get; set; }

        [StringLength(15, ErrorMessage = "PasswordLength", MinimumLength = 5)]
        [Required(ErrorMessage = "PasswordRequired")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
