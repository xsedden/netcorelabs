﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.ViewModels
{
    public class CreateCommentsModel
    {
        [Required(ErrorMessage = "TextRequired")]
        [StringLength(20, ErrorMessage = "TextLength", MinimumLength = 6)]
        public string Text { get; set; }
    }
}
