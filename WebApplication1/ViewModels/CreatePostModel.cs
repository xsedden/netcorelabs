﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace WebApplication1.ViewModels
{
    public class CreatePostModel
    {
        [StringLength(20, ErrorMessage = "NameLength", MinimumLength = 6)]
        [Required(ErrorMessage = "NameRequired")]
        public string Name { get; set; }

        [Required(ErrorMessage = "TextRequired")]
        public string Text { get; set; }       
    }
}
