﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;
using WebApplication1.Models;
using WebApplication1.ViewModels;
using BlogHost.BLL.DTO;

namespace WebApplication1.TagHelpers
{
    [HtmlTargetElement("show-comment")]
    public class CommentsTagHelper : TagHelper
    {
        public ViewCommentsModel Com { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "CommentInfo";
            string postInfoContent = $@"<tr>
                                        <td>{Com.Text}</td>
                                        <td>{Com.CreationDate}</td>
                                        <td>{Com.Author}</td>
                                     </tr>";
            output.Content.SetHtmlContent(postInfoContent);

        }
    }
}
          /* <p>
            <td>@Html.DisplayFor(modelItem => item.Text)</td>
            <td>    @Html.DisplayFor(modelItem => item.CreationDate)</td>
        </p>*/