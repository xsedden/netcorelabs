﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ninject.Modules;
using BlogHost.BLL.Services;
using BlogHost.BLL.Interfaces;

namespace WebApplication1.Util
{
    public class WEBModule : NinjectModule
    {
        /// <summary>
        /// Trash
        /// </summary>
        public override void Load()
        {
            Bind<IBlogService>().To<BlogService>();
            Bind<ITagService>().To<TagService>();
            Bind<IPostService>().To<PostService>();
            Bind<ICommentService>().To<CommentService>();
            Bind<IUserService>().To<UserService>();
        }
    }
}
