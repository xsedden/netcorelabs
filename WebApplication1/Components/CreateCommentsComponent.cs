﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;

namespace WebApplication1.Components
{
    public class CreateCommentsComponent //:// ViewComponent
    {
        /*private UserContext db;

        public CreateCommentsComponent(UserContext context)
        {
            db = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(CreateCommentsModel model, int id)
        {
            if (ModelState.IsValid)
            {
                Comments com = new Comments { Text = model.Text, CreationDate = DateTime.Now, PostId = id };
                db.Comments.Add(com);
                await db.SaveChangesAsync();
            }
            return View(model);

        <!--
<table class="table">
    <thead>
        <tr>
            <th>
                @Html.DisplayNameFor(model => model.Name)
            </th>
            <th>
                @Html.DisplayNameFor(model => model.CreationDate)
            </th>
            <th>
                @Html.DisplayNameFor(model => model.Author)
            </th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach (var item in Model)
        {
            <tr>
                <td>
                    <a asp-controller="Posts" asp-action="Index" asp-route-id="@item.Id">
                        @Html.DisplayFor(modelItem => item.Name)
                    </a>
                </td>
                <td>
                    @Html.DisplayFor(modelItem => item.CreationDate)
                </td>
                <td>
                    @Html.DisplayFor(modelItem => item.Author)
                </td>
                <td>|
                    <a asp-action="Delete" asp-route-id="@item.Id">Delete</a>
                </td>
            </tr>
        }
    </tbody>
</table>-->

        <table class="table">
    <thead>
        <tr>
            <th>
                @Html.DisplayNameFor(model => model.Name)
            </th>
            <th>
                @Html.DisplayNameFor(model => model.CreationDate)
            </th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach (var item in Model)
        {
            <tr>
                <td>
                    <a asp-action="Info" asp-route-id="@item.Id">@Html.DisplayFor(modelItem => item.Name)</a>
                </td>
                <td>
                    @Html.DisplayFor(modelItem => item.CreationDate)
                </td>
                <td>
                    <a asp-action="Delete" asp-route-id="@item.Id">Delete</a>
                </td>
                <td>
                    <a asp-action="Edit" asp-route-id="@item.Id">Edit</a>
                </td>
            </tr>
        }
    </tbody>
</table>
        }*/
    }
}
