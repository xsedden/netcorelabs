﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using BlogHost.BLL.Interfaces;
using BlogHost.BLL.DTO;
using AutoMapper;

namespace WebApplication1.Components
{
    public class CommentsComponents : ViewComponent
    {
        IUnit db { get; set; }

        public CommentsComponents(IUnit uow)
        {
            db = uow;
        }

        public IViewComponentResult Invoke(int id)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<CommentDTO, ViewCommentsModel>().ForMember("Author", opt => opt.MapFrom(c => db.Users.GetUserById((int)c.UserId).Login))).CreateMapper();
            return View(mapper.Map<IEnumerable<CommentDTO>, List<ViewCommentsModel>>(db.Comments.GetCommentsForPost(id)));
        }
    }
}
