﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.BLL.DTO;

namespace BlogHost.BLL.Interfaces
{
    public interface ICommentService
    {
        List<CommentDTO> GetCommentsForPost(int postId);

        List<CommentDTO>  GetCommentsByAuthor(int id);

        void SaveComment(CommentDTO comment);

        void DeleteComment(int id);
    }
}
