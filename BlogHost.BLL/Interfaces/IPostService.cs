﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.BLL.DTO;

namespace BlogHost.BLL.Interfaces
{
    public interface IPostService
    {
        PostDTO GetPostById(int id);

        List<PostDTO> GetPostsByTag(int id);

        List<PostDTO> GetPostsBySearchRequest(string request);

        List<PostDTO> GetPostsByAuthor(int id);

        List<PostDTO> GetPostsByBlog(int id);

        List<PostDTO> GetPostsAll();

        void UpdatePost(PostDTO post);

        void SavePost(PostDTO post);

        void DeletePost(int id);
    }
}
