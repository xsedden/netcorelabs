﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.BLL.DTO;

namespace BlogHost.BLL.Interfaces
{
    public interface ITagService
    {
        List<TagDTO> GetTagsForPost(int postId);

        TagDTO GetTagByName(string name);

        TagDTO GetTagById(int id);

        void SaveTag(TagDTO post);

        void DeleteTag(int id);

        //List<TagDTO> GetTagsBySearchRequest(string request);
    }
}
