﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.BLL.DTO;


namespace BlogHost.BLL.Interfaces
{
    public interface IRoleService
    {
        RoleDTO GetRoleAdmin();
        RoleDTO GetRoleUser();
    }
}
