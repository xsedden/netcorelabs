﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.BLL.DTO;

namespace BlogHost.BLL.Interfaces
{
    public interface IUserService
    {
        UserDTO GetUserById(int id);

        //UserDTO GetUserByEmail(string email);

        //DbUserDTO GetDbUserById(string id);

        //List<UserDTO> GetUsersBySearchRequest(string request);

        //void UpdateUser(UserDTO user);

        List<UserDTO> GetAllUsers();

        List<UserDTO> GetAdmins();

        UserDTO AuthLogin(string pass, string email);

        int GetIdByEmail(string email);

        void SaveUser(UserDTO user);
    }
}
