﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.BLL.DTO;

namespace BlogHost.BLL.Interfaces
{
    public interface IBlogService
    {
        BlogDTO GetBlogByID(int id);

        List<BlogDTO> GetBlogsBySearchRequest(string request);

        List<BlogDTO> GetBlogsForAuthor(int id);

        void SaveBlog(BlogDTO blog);

        void UpdateBlog(BlogDTO blog);

        void DeleteBlog(int id);

        List<BlogDTO> GetAllBlogs();
    }
}
