﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.BLL.Interfaces;

namespace BlogHost.BLL.Interfaces
{
    public interface IUnit : IDisposable
    {
        IBlogService Blogs { get; }
        ICommentService Comments { get; }
        IPostService Posts { get; }
        IRoleService Roles { get; }
        ITagService Tags { get; }
        IUserService Users { get; }
    }
}
