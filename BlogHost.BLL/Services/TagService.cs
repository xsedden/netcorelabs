﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using BlogHost.BLL.Interfaces;
using BlogHost.BLL.DTO;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;
using System.Linq;

namespace BlogHost.BLL.Services
{
    public class TagService : ITagService
    {
        IUnitOfWork db { get; set; }

        public TagService(IUnitOfWork uow)
        {
            db = uow;
        }

        public List<TagDTO> GetTagsForPost(int postId)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Tag, TagDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Tag>, List<TagDTO>>(db.Tags.GetTagsByPost(postId));
        }

        public TagDTO GetTagByName(string name)
        {
            var tag = db.Tags.GetTagByName(name);
            return new TagDTO { Id = tag.Id, Name = tag.Name, PostId = tag.PostId };
        }

        public TagDTO GetTagById(int id)
        {
            var tag = db.Tags.Get(id);
            return new TagDTO { Id = tag.Id, Name = tag.Name, PostId = tag.PostId };
        }


        public void SaveTag(TagDTO tag)
        {
            db.Tags.Create(new Tag { Name = tag.Name, PostId = tag.PostId});

        }

        public void DeleteTag(int id)
        {
            db.Tags.Delete(id);
        }

    }
}
