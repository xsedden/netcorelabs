﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using BlogHost.BLL.Interfaces;
using BlogHost.BLL.DTO;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;
using System.Linq;

namespace BlogHost.BLL.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork db { get; set; }

        public UserService(IUnitOfWork uow)
        {
            db = uow;
        }

        public int GetIdByEmail(string email)
        {
            return db.Users.GetAll().FirstOrDefault(u => u.Email == email).Id;
        }

        public UserDTO AuthLogin(string pass, string email)
        {
            var user = db.Users.GetAll().FirstOrDefault(u => u.Email == email && u.Password == pass);
            if (user != null) 
                return new UserDTO { Id = user.Id, Email = user.Email, Login = user.Login, Password = user.Password, RoleId = user.RoleId, BlogsIds = user.Blogs.Select(u => u.Id), CommentsIds = user.Comments.Select(u => u.Id), PostsIds = user.Posts.Select(u => u.Id) };
            return new UserDTO { };
        }

        public UserDTO GetUserById(int id)
        {
            var user = db.Users.Get(id);
            return new UserDTO { Id = id, Email = user.Email, Login = user.Login, Password = user.Password, RoleId = user.RoleId, BlogsIds = user.Blogs.Select(u => u.Id), CommentsIds = user.Comments.Select(u => u.Id), PostsIds = user.Posts.Select(u => u.Id) };
        }

        public void SaveUser(UserDTO user)
        {
            db.Users.Create(new User { Email = user.Email, Login = user.Login, Password = user.Password, RoleId = user.RoleId  });

        }
        /*
        public UserDTO GetUserByEmail(string email)
        {
            var user = db.Users.
        }*/
        /*
        public List<UserDTO> GetUsersBySearchRequest(string request)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<User, UserDTO>().ForMember("TagsIds", opt => opt.MapFrom(c => c.Tags.Select(u => u.Id))).ForMember("CommentsIds", opt => opt.MapFrom(c => c.Comments.Select(u => u.Id)))).CreateMapper();
            return mapper.Map<IEnumerable<User>, List<UserDTO>>(db.Users.);
        }
        */
        /*
        public void UpdateUser(UserDTO user)
        {

        }*/

        public List<UserDTO> GetAllUsers()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<User, UserDTO>().ForMember("PostsIds", opt => opt.MapFrom(c => c.Posts.Select(u => u.Id))).ForMember("BlogsIds", opt => opt.MapFrom(c => c.Blogs.Select(u => u.Id))).ForMember("CommentsIds", opt => opt.MapFrom(c => c.Comments.Select(u => u.Id)))).CreateMapper();
            return mapper.Map<IEnumerable<User>, List<UserDTO>>(db.Users.GetAll());
        }

        public List<UserDTO> GetAdmins()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<User, UserDTO>().ForMember("PostsIds", opt => opt.MapFrom(c => c.Posts.Select(u => u.Id))).ForMember("BlogsIds", opt => opt.MapFrom(c => c.Blogs.Select(u => u.Id))).ForMember("CommentsIds", opt => opt.MapFrom(c => c.Comments.Select(u => u.Id)))).CreateMapper();
            return mapper.Map<IEnumerable<User>, List<UserDTO>>(db.Users.GetAdmins());
        }
    }
}
