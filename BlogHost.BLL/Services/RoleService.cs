﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using BlogHost.BLL.Interfaces;
using BlogHost.BLL.DTO;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;
using System.Linq;

namespace BlogHost.BLL.Services
{
    public class RoleService : IRoleService
    {
        IUnitOfWork db { get; set; }

        public RoleService(IUnitOfWork uow)
        {
            db = uow;
        }

        public RoleDTO GetRoleAdmin()
        {
            var role = db.Roles.GetAdminRole();
            List<User> uids = db.Users.GetAdmins().ToList();
            IEnumerable<int> ids = uids.Select(u => u.Id);
            return new RoleDTO {  Id = role.Id, Name = role.Name, UsersIds= ids};
        }

        public RoleDTO GetRoleUser()
        {
            var role = db.Roles.GetUserRole();
            List<User> uids = db.Users.GetUsers().ToList();
            IEnumerable<int> ids = uids.Select(u => u.Id);
            return new RoleDTO { Id = role.Id, Name = role.Name, UsersIds = ids };
        }
    }
}
