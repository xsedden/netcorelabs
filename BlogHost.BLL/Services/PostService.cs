﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using BlogHost.BLL.Interfaces;
using BlogHost.BLL.DTO;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;
using System.Linq;

namespace BlogHost.BLL.Services
{
    public class PostService : IPostService
    {
        IUnitOfWork db { get; set; }

        public PostService(IUnitOfWork uow)
        {
            db = uow;
        }

        public PostDTO GetPostById(int id)
        {
            var post = db.Posts.Get(id);
            return new PostDTO { Id = id, BlogId = post.BlogId, CreationDate = post.CreationDate, Name = post.Name, Text = post.Text, UserId = post.UserId, CommentsIds = post.Comments.Select(u => u.Id) };
        }

        public List<PostDTO> GetPostsByTag(int id)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Post, PostDTO>().ForMember("TagsIds", opt => opt.MapFrom(c => c.Tags.Select(u => u.Id))).ForMember("CommentsIds", opt => opt.MapFrom(c => c.Comments.Select(u => u.Id)))).CreateMapper();
            return mapper.Map<IEnumerable<Post>, List<PostDTO>>(db.Posts.GetPostsForTag(id));
        }

        public List<PostDTO> GetPostsBySearchRequest(string request)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Post, PostDTO>().ForMember("TagsIds", opt => opt.MapFrom(c => c.Tags.Select(u => u.Id))).ForMember("CommentsIds", opt => opt.MapFrom(c => c.Comments.Select(u => u.Id)))).CreateMapper();
            return mapper.Map<IEnumerable<Post>, List<PostDTO>>(db.Posts.GetPostsBySearchRequest(request));
        }

        public List<PostDTO> GetPostsByAuthor(int id)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Post, PostDTO>().ForMember("TagsIds", opt => opt.MapFrom(c => c.Tags.Select(u => u.Id))).ForMember("CommentsIds", opt => opt.MapFrom(c => c.Comments.Select(u => u.Id)))).CreateMapper();
            return mapper.Map<IEnumerable<Post>, List<PostDTO>>(db.Posts.GetPostsForAuthor(id));
        }

        public List<PostDTO> GetPostsByBlog(int id)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Post, PostDTO>().ForMember("TagsIds", opt => opt.MapFrom(c => c.Tags.Select(u => u.Id))).ForMember("CommentsIds", opt => opt.MapFrom(c => c.Comments.Select(u => u.Id)))).CreateMapper();
            return mapper.Map<IEnumerable<Post>, List<PostDTO>>(db.Posts.GetPostsForBlog(id));

        }

        public List<PostDTO> GetPostsAll()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Post, PostDTO>().ForMember("TagsIds", opt => opt.MapFrom(c => c.Tags.Select(u => u.Id))).ForMember("CommentsIds", opt => opt.MapFrom(c => c.Comments.Select(u => u.Id)))).CreateMapper();
            return mapper.Map<IEnumerable<Post>, List<PostDTO>>(db.Posts.GetAll());
        }


        public void UpdatePost(PostDTO post)
        {
            db.Posts.Update(new Post { Name = post.Name, Text = post.Text, });
        }

        public void SavePost(PostDTO post)
        {
            db.Posts.Create(new Post { Name = post.Name, BlogId = post.BlogId, CreationDate = DateTime.Now, Text = post.Text, UserId = post.UserId, });

        }

        public void DeletePost(int id)
        {
            db.Posts.Delete(id);
        }
    }
}
