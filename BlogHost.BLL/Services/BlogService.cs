﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using BlogHost.BLL.Interfaces;
using BlogHost.BLL.DTO;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;
using System.Linq;



namespace BlogHost.BLL.Services
{
    public class BlogService : IBlogService
    {
        IUnitOfWork db { get; set; }

        public BlogService(IUnitOfWork uow)
        {
            db = uow;
        }

        public List<BlogDTO> GetAllBlogs()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Blog, BlogDTO>().ForMember("PostsIds", opt => opt.MapFrom(c => c.Posts.Select(u => u.Id)))).CreateMapper();
            return mapper.Map<IEnumerable<Blog>, List<BlogDTO>>(db.Blogs.GetAll());
        }

        public BlogDTO GetBlogByID(int id)
        {
            var blog = db.Blogs.Get(id);
            List<int> postid = new List<int>();
            foreach (var a in db.Posts.GetPostsForBlog(id))
                postid.Add(a.Id);
            return new BlogDTO { Id = id, CreationDate = blog.CreationDate, Name = blog.Name, UserId = blog.UserId, PostsIds = postid };
        }

        public List<BlogDTO> GetBlogsBySearchRequest(string request)
        {
            //var blogs = db.Blogs.GetBlogsBySearchRequest(request);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Blog, BlogDTO>().ForMember("PostsIds", opt => opt.MapFrom(c => c.Posts.Select(u => u.Id)))).CreateMapper();
            return mapper.Map<IEnumerable<Blog>, List<BlogDTO>>(db.Blogs.GetBlogsBySearchRequest(request));

        }

        public List<BlogDTO> GetBlogsForAuthor(int id)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Blog, BlogDTO>().ForMember("PostsIds", opt => opt.MapFrom(c => c.Posts.Select(u=> u.Id)))).CreateMapper();
            return mapper.Map<IEnumerable<Blog>, List<BlogDTO>>(db.Blogs.GetBlogsForAuthor(id));
        }

        public void SaveBlog(BlogDTO blog)
        {
            db.Blogs.Create(new Blog { Name = blog.Name, CreationDate = DateTime.Now, UserId = blog.UserId, });
        }
        //
        public void UpdateBlog(BlogDTO blog)
        {
            db.Blogs.Update(new Blog { Name = blog.Name, CreationDate = DateTime.Now, UserId = blog.UserId,  });// изменить апдейт по ид
        }
        //
        public void DeleteBlog(int id)
        {
            db.Blogs.Delete(id);
        }
    }
}
