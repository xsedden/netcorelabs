﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using BlogHost.BLL.Interfaces;
using BlogHost.BLL.DTO;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;
using System.Linq;

namespace BlogHost.BLL.Services
{
    public class CommentService : ICommentService
    {
        IUnitOfWork db { get; set; }

        public CommentService(IUnitOfWork uow)
        {
            db = uow;
        }

        public List<CommentDTO> GetCommentsForPost(int postId)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Comment, CommentDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Comment>, List<CommentDTO>>(db.Comments.GetCommentsByPost(postId));
        }

        public List<CommentDTO> GetCommentsByAuthor(int id)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Comment, CommentDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Comment>, List<CommentDTO>>(db.Comments.GetCommentsByAuthor(id));
        }

        public void SaveComment(CommentDTO com)
        {
            db.Comments.Create(new Comment {  CreationDate = DateTime.Now, PostId = com.PostId, Text = com.Text, UserId = com.UserId});
        }

        public void DeleteComment(int id)
        {
            db.Comments.Delete(id);
        }
    }
}
