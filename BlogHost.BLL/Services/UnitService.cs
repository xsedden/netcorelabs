﻿using System;
using System.Collections.Generic;
using System.Text;
using BlogHost.BLL.Interfaces;
using BlogHost.BLL.DTO;
using BlogHost.DAL.Interfaces;
using BlogHost.DAL.Entities;

namespace BlogHost.BLL.Services
{
    public class UnitService : IUnit
    {
        //private UserContext db;
        private  BlogService blogRepository;
        private PostService postRepository;
        private CommentService comRepository;
        private TagService tagRepository;
        private UserService userRepository;
        private RoleService roleRepository;

        /*public EFUnitOfWork(string connectionString)
        {
            db = new UserContext(connectionString);
        }*/

        private readonly IUnitOfWork Db;

        public UnitService(IUnitOfWork uow)
        {
            Db = uow;
        }

        public ITagService Tags
        {
            get
            {
                if (tagRepository == null)
                    tagRepository = new TagService(Db);
                return tagRepository;
            }
        }

        public IRoleService Roles
        {
            get
            {
                if (roleRepository == null)
                    roleRepository = new RoleService(Db);
                return roleRepository;
            }
        }

        public ICommentService Comments
        {
            get
            {
                if (comRepository == null)
                    comRepository = new CommentService(Db);
                return comRepository;
            }
        }

        public IBlogService Blogs
        {
            get
            {
                if (blogRepository == null)
                    blogRepository = new BlogService(Db);
                return blogRepository;
            }
        }

        public IPostService Posts
        {
            get
            {
                if (postRepository == null)
                    postRepository = new PostService(Db);
                return postRepository;
            }
        }

        public IUserService Users
        {
            get
            {
                if (userRepository == null)
                    userRepository = new UserService(Db);
                return userRepository;
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
