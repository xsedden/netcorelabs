﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogHost.BLL.DTO
{
    public class BlogDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }

        public int? UserId { get; set; }

        public virtual IEnumerable<int> PostsIds { get; set; }

        public BlogDTO()
        {
            PostsIds = new List<int>();
        }
    }
}
