﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogHost.BLL.DTO
{
    public class TagDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? PostId { get; set; }
    }
}
