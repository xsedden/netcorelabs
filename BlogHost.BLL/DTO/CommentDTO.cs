﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogHost.BLL.DTO
{
    public class CommentDTO
    {
        public int Id { get; set; }
        public string Text { get; set; }

        public int? PostId { get; set; }

        public int? UserId { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
