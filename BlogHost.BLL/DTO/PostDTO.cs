﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogHost.BLL.DTO
{
    public class PostDTO
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }

        public int? BlogId { get; set; }

        public int? UserId { get; set; }

        public virtual IEnumerable<int> CommentsIds { get; set; }
        public virtual IEnumerable<int> TagsIds { get; set; }

        public PostDTO()
        {
            CommentsIds = new List<int>();
            TagsIds = new List<int>();
        }
    }
}
