﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogHost.BLL.DTO
{
    public class RoleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual IEnumerable<int> UsersIds { get; set; }
        public RoleDTO()
        {
            UsersIds = new List<int>();
        }
    }
}
