﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogHost.BLL.DTO
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public int? RoleId { get; set; }

        public virtual IEnumerable<int> BlogsIds { get; set; }
        public virtual IEnumerable<int> PostsIds { get; set; }
        public virtual IEnumerable<int> CommentsIds { get; set; }
        public UserDTO()
        {
            CommentsIds = new List<int>();
            BlogsIds = new List<int>();
            PostsIds = new List<int>();
        }
    }
}
